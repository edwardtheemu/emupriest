﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Forms;
using CustomClassTemplate.Gui;
using CustomClassTemplate.Objects;
using CustomClassTemplate.Settings;
using ZzukBot.Constants;
using ZzukBot.ExtensionFramework.Classes;
using ZzukBot.Game.Statics;
using ZzukBot.Mem;
using ZzukBot.Objects;

// EmuPriest - 99% Credits to z0mg
// Would like to give a huge thanks to z0mg (Rob)
// I used his doctorDeath CC as a template
// very easy to follow and adapt - Thanks Heaps!

namespace CustomClassTemplate
{
    [Export(typeof(CustomClass))]
    public class EmuClass : CustomClass
    {
        private Form CCGui = new CCGui();
        private Spellbook spellbook;
        /// <summary>
        /// The WoW class the CustomClass is designed for
        /// </summary>
        public override Enums.ClassId Class => Enums.ClassId.Priest;

        /// <summary>
        /// Should be called when the CC is loaded
        /// </summary>
        /// <returns></returns>
        public override bool Load()
        {
            this.spellbook = new Spellbook();
            CustomClassSettings.Values.Load();
            return true;
        }

        /// <summary>
        /// Should be called when the CC is unloaded
        /// </summary>
        public override void Unload()
        {
        }

        /// <summary>
        /// Should be called when the botbase is pulling an unit
        /// </summary>
        public override void OnPull()
        {
            try
            {
                WoWUnit targetUnit = ObjectManager.Instance.Target;
                if (targetUnit == null) return;


                var damageSpells = this.spellbook.GetDamageSpells();

                foreach (var spell in damageSpells)
                {
                    if (spell.IsWanted)
                    {
                        spell.Cast();
                        spellbook.UpdateLastSpell(spell);
                        break;
                    }
                }
            }
            catch (Exception e)
            {

            }
        }

        /// <summary>
        /// Should be called when the botbase is fighting an unit
        /// </summary>
        public override void OnFight()
        {
            try
            {
                WoWUnit targetUnit = ObjectManager.Instance.Target;
                if (targetUnit == null) return;

                var damageSpells = this.spellbook.GetDamageSpells();

                foreach (var spell in damageSpells)
                {
                    if (spell.IsWanted)
                    {
                        spell.Cast();
                        spellbook.UpdateLastSpell(spell);
                        break;
                    }
                }
            }
            catch(Exception e)
            {
                
            }
        }

        /// <summary>
        /// Should be called when the botbase is resting
        /// </summary>
        public override void OnRest()
        {
            //This needs to be tested & cleaned up
            MainThread.Instance.Invoke(() =>
            {
                try
                {
                    if (!ObjectManager.Instance.Player.IsDrinking)
                    {
                        ObjectManager.Instance.Items.FirstOrDefault(i => i.Name == CustomClassSettings.Values.DrinkName)
                            .Use();
                        ZzukBot.Helpers.Wait.For("DrinkPriest", 500);
                    }
                    if (!ObjectManager.Instance.Player.IsEating)
                    {
                        ObjectManager.Instance.Items.FirstOrDefault(i => i.Name == CustomClassSettings.Values.FoodName)
                            .Use();
                        ZzukBot.Helpers.Wait.For("EatPriest", 500);
                    }
                }
                catch
                {

                }
            });
        }

        /// <summary>
        /// Should be called when the botbase is resting
        /// </summary>
        /// <returns>
        /// Returns true when the character is done buffing
        /// </returns>
        public override bool OnBuff()
        {
            try
            {
                var buffs = this.spellbook.GetBuffSpells();

                foreach (var spell in buffs)
                {
                    if (spell.IsWanted)
                    {
                        spell.Cast();
                        return false;
                    }
                }
            }
            catch (Exception e)
            {

            }
            return true;
        }

        /// <summary>
        /// Should be called to show the settings form
        /// </summary>
        public override void ShowGui()
        {
            CCGui.Dispose();
            CCGui = new CCGui();
            CCGui.Show();
        }

        /// <summary>
        /// The name of the CC
        /// </summary>
        public override string Name => "EmuPriest";

        /// <summary>
        /// The author of the CC
        /// </summary>
        public override string Author => "z0mg";

        /// <summary>
        /// The version of the CC
        /// </summary>
        public override int Version => 1;

        /// <summary>
        /// The current combat distance
        /// </summary>
        public override float CombatDistance => Data.Statics.CombatDistance;

    }
}
