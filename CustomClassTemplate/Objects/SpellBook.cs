﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ZzukBot.ExtensionFramework.Classes;
using ZzukBot.Game.Statics;
using ZzukBot.Objects;
using CustomClassTemplate.Data;

namespace CustomClassTemplate.Objects
{
    internal class Spellbook
    {
        private static Spell lastSpell = new Spell(string.Empty, -1, false, false);

        private List<Spell> spells;


        // Survival Spells - Not buff but doesn't do damage
        public static readonly Spell Sheild = new Spell("Power Word: Shield", 1500, false, false,
            isWanted:
                () =>
                    Helpers.CanCast("Power Word: Shield") &&
                    Me.ManaPercent >= 20 && Target.HealthPercent >= 10 &&
                    !Me.GotDebuff("Weakened Soul"), customAction:
                () =>
                {
                    //Make sure it's cast on us!
                    Data.Helpers.TryBuff("Power Word: Shield");
                });

        public static readonly Spell LesserHeal = new Spell("Lesser Heal", 1500, false, false,
            isWanted:
                () =>
                    Helpers.CanCast("Lesser Heal") &&
                    Me.ManaPercent >= 15 && Me.HealthPercent <= 40 &&
                    Target.HealthPercent >= 5 && !Helpers.CanCast("Flash Heal"), customAction:
                () =>
                {
                    //Make sure it's cast on us!
                    Data.Helpers.TryBuff("Lesser Heal");
                });
        public static readonly Spell FlashHeal = new Spell("Flash Heal", 1400, false, false,
            isWanted:
                () =>
                    Helpers.CanCast("Flash Heal") &&
                    Me.ManaPercent >= 15 && Me.HealthPercent <= 40 &&
                    Target.HealthPercent >= 5, customAction:
                () =>
                {
                            //Make sure it's cast on us!
                            Data.Helpers.TryBuff("Flash Heal");
                });

        // Damage Spells
        public static readonly Spell MindBlast = new Spell("Mind Blast", 1000, false, true,
            isWanted:
                () =>
                    Helpers.CanCast("Mind Blast") &&
                    Me.ManaPercent >= 40 && Target.HealthPercent >= 10);

        public static readonly Spell MindFlay = new Spell("Mind Flay", 850, false, true, false, isChanneled: true,
            isWanted:
                () =>
                    Helpers.CanCast("Mind Flay") && Target.GotDebuff("Shadow Word: Pain") &&
                    Me.ManaPercent >= 40 && Target.HealthPercent >= 20);

        public static readonly Spell SWP = new Spell("Shadow Word: Pain", 900, false, true, true,
            isWanted:
                () =>
                    Target.HealthPercent >= 10 && !Target.GotDebuff("Shadow Word: Pain") &&
                    Data.Helpers.CanCast("Shadow Word: Pain"));

        public static readonly Spell DevouringPlague = new Spell("Devouring Plague", 950, false, true, true,
            isWanted:
                () =>
                    Target.HealthPercent >= 85 && !Target.GotDebuff("Devouring Plague") &&
                    Data.Helpers.CanCast("Devouring Plague"));

        public static readonly Spell Smite = new Spell("Smite", 250, false, true,
            isWanted:
                () =>
                    !Helpers.CanCast("Mind Blast") && !Helpers.CanWand() &&
                    Me.ManaPercent >= 20);

        // Buff spells.
        public static readonly Spell PowerWordFort = new Spell("Power Word: Fortitude", 10, true, false,
            isWanted:
                () =>
                    Helpers.ShouldBuffSelf("Power Word: Fortitude"));

        public static readonly Spell InnerFire = new Spell("Inner Fire", 9, true, false,
            isWanted:
                () =>
                    Helpers.ShouldBuffSelf("Inner Fire"));

        public static readonly Spell TouchOfWeakness = new Spell("Touch of Weakness", 8, true, false,
            isWanted:
                () =>
                    Helpers.ShouldBuffSelf("Touch of Weakness"));

        public static readonly Spell DivineSpirit = new Spell("Divine Spirit", 12, true, false,
            isWanted:
                () =>
                    Helpers.ShouldBuffSelf("Divine Spirit"));

        // Misc
        public static readonly Spell Wand = new Spell("Shoot", 300, false, false, true, true, isWanted: () =>
        {
            return Helpers.CanWand() && Target.GotDebuff("Shadow Word: Pain") && (Me.GotAura("Power Word: Shield") || Me.GotDebuff("Weakened Soul"));
        }, customAction: () => ZzukBot.Game.Statics.Spell.Instance.StartWand());

        public static readonly Spell Attack = new Spell("Attack", Int32.MinValue, false, true, true, false,
            () => !Helpers.CanWand() && Helpers.CanCast("Attack"), customAction:
                () =>
                {
                    ZzukBot.Game.Statics.Spell.Instance.Attack();
                    Statics.CombatDistance = 4;
                });

        public Spellbook()
        {
            this.spells = new List<Spell>();
            this.InitializeSpellbook();
        }

        public IEnumerable<Spell> GetDamageSpells()
        {
            return Cache.Instance.GetOrStore("damageSpells", () => this.spells.Where(s => !s.IsBuff));
        }

        public IEnumerable<Spell> GetBuffSpells()
        {
            return Cache.Instance.GetOrStore("buffSpells", () => this.spells.Where(s => s.IsBuff && !s.DoesDamage));
        }

        public void UpdateLastSpell(Spell spell)
        {
            lastSpell = spell;
        }

        private void InitializeSpellbook()
        {
            foreach (var property in this.GetType().GetFields())
            {
                spells.Add(property.GetValue(property) as Spell);
            }

            spells = spells.OrderByDescending(s => s.Priority).ToList();
        }

        private static WoWUnit Me
        {
            get { return ObjectManager.Instance.Player; }
        }

        private static WoWUnit Target
        {
            get { return ObjectManager.Instance.Target; }
        }

        private static WoWUnit Pet
        {
            get { return ObjectManager.Instance.Pet; }
        }
    }
}
